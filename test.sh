#!/bin/sh

# Script for testing VMS
# Warning: these calculations can me very time demanding

###########################################################################
# SETUP
###########################################################################
DB="../00000001_00025000.xyz"
REQUEST=$(mktemp /tmp/request.$$.XXXXXX)
OPT="optimize"
EXC="excited"
BASIS="def2-SVP def2-TZVP def2-QZVP cc-pVDZ cc-pVTZ cc-pVQZ"

# Default methods for testing
DFTOPTdef="BP86"
DFTEXCdef="BP86"
SEMIOPTdef="ZINDO/1"
SEMIEXCdef="ZINDO/S"

# Other default values
BASISdef="def2-SVP"
NROOTSdef=5
Ndef=1

# Testing borders
Jmax=12
MEMORYmin=16
MEMORYmax=8192
MAXDIMmin=1
MAXDIMmax=20
NROOTSmax=20
IROOTmax=20

# Maximum number of molecules for testing scaling with number of molecules
NDFTOPTmax=0
NDFTEXCmax=4096
NSEMIOPTmax=4096
NSEMIEXCmax=0

# Number of molecules for -j testing
NDFTOPTj=0
NDFTEXCj=1024
NSEMIOPTj=4096
NSEMIEXCj=0
###########################################################################

run() {
	time -p vms -l $1 -j $2 -w $DB $REQUEST $MAINDIR/$3 > $MAINDIR/$3.log 2>&1
}

dir() {
	MAINDIR=$1
	rm -rf $MAINDIR && mkdir $MAINDIR
}

setDFTEXC() {
cat << __heredoc__ > $REQUEST
job     $EXC
method  $DFTEXCdef
basis   $BASISdef
nroots  $NROOTSdef
absorb	300 400 0.40 -1
__heredoc__
}

setDFTOPT() {
cat << __heredoc__ > $REQUEST
job     $OPT
method  $DFTOPTdef
basis   $BASISdef
__heredoc__
}

setSEMIEXC() {
cat << __heredoc__ > $REQUEST
job     $EXC
method	$SEMIEXCdef
nroots	$NROOTSdef
absorb	300 400 0.40 -1
__heredoc__
}

setSEMIOPT() {
cat << __heredoc__ > $REQUEST
job     $OPT
method	$SEMIOPTdef
__heredoc__
}

testN() {
	dir "TEST_N"

	setDFTEXC
	N=1
	while test $N -le $NDFTEXCmax
	do
		run $N $Jmax "$EXC.dft.N.$N"
		N=$((N*2))
	done

	setDFTOPT
	N=1
	while test $N -le $NDFTOPTmax
	do
		run $N $Jmax "$OPT.dft.N.$N"
		N=$((N*2))
	done

	setSEMIEXC
	N=1
	while test $N -le $NSEMIEXCmax
	do
		run $N $Jmax "$EXC.semi.N.$N"
		N=$((N*2))
	done

	setSEMIOPT
	N=1
	while test $N -le $NSEMIOPTmax
	do
		run $N $Jmax "$OPT.semi.N.$N"
		N=$((N*2))
	done
}

testJ() {
	dir "TEST_J"

	setDFTEXC
	J=1
	while test $J -le $Jmax
	do
		run $NDFTEXCj $J "$EXC.dft.J.$J"
		J=$((J+1))
	done

	setDFTOPT
	J=1
	while test $J -le $Jmax
	do
		run $NDFTOPTj $J "$OPT.dft.J.$J"
		J=$((J+1))
	done

	setSEMIEXC
	J=1
	while test $J -le $Jmax
	do
		run $NSEMIEXCj $J "$EXC.semi.J.$J"
		J=$((J+1))
	done

	setSEMIOPT
	J=1
	while test $J -le $Jmax
	do
		run $NSEMIOPTj $J "$OPT.semi.J.$J"
		J=$((J+1))
	done
}

testBASIS() {
	dir "TEST_BASIS"
	for B in $BASIS 
	do
		setDFTEXC
		echo "basis	$B" >> $REQUEST
		run $Ndef 1 "$EXC.dft.BASIS.$B"
		setDFTOPT
		echo "basis	$B" >> $REQUEST
		run $Ndef 1 "$OPT.dft.BASIS.$B"
	done
}

testMEMORY() {
	dir "TEST_MEMORY"
	MEMORY=$MEMORYmin
	while test $MEMORY -le $MEMORYmax
	do
		setDFTEXC
		echo "memory	$MEMORY" >> $REQUEST
		run $Ndef 1 "$EXC.dft.MEMORY.$MEMORY"
		setDFTOPT
		echo "memory	$MEMORY" >> $REQUEST
		run $Ndef 1 "$OPT.dft.MEMORY.$MEMORY"
		setSEMIEXC
		echo "memory	$MEMORY" >> $REQUEST
		run $Ndef 1 "$EXC.semi.MEMORY.$MEMORY"
		setSEMIOPT
		echo "memory	$MEMORY" >> $REQUEST
		run $Ndef 1 "$OPT.semi.MEMORY.$MEMORY"
		MEMORY=$((MEMORY*2))
	done
}

testMAXDIM() {
	dir "TEST_MAXDIM"
	MAXDIM=$MAXDIMmin
	while test $MAXDIM -le $MAXDIMmax
	do
		setDFTEXC
		echo "maxdim	$MAXDIM" >> $REQUEST
		run $Ndef 1 "$EXC.dft.MAXDIM.$MAXDIM"
		setSEMIEXC
		echo "maxdim	$MAXDIM" >> $REQUEST
		run $Ndef 1 "$EXC.semi.MAXDIM.$MAXDIM"
		MAXDIM=$((MAXDIM+1))
	done
}

testNROOTS() {
	dir "TEST_NROOTS"
	NROOTS=1
	while test $NROOTS -le $NROOTSmax
	do
		setDFTEXC
		echo "nroots	$NROOTS" >> $REQUEST
		run $Ndef 1 "$EXC.dft.NROOTS.$NROOTS"
		setSEMIEXC
		echo "nroots	$NROOTS" >> $REQUEST
		run $Ndef 1 "$EXC.semi.NROOTS.$NROOTS"
		NROOTS=$((NROOTS+1))
	done
}

testIROOT() {
	dir "TEST_IROOT"
	IROOT=0
	while test $IROOT -le $IROOTmax
	do
		setDFTOPT
		echo "iroot	$IROOT" >> $REQUEST
		run $Ndef 1 "$EXC.dft.IROOT.$IROOT"
		setSEMIOPT
		echo "iroot	$IROOT" >> $REQUEST
		run $Ndef 1 "$EXC.semi.IROOT.$IROOT"
		IROOT=$((IROOT+1))
	done
}

testN
testJ
testBASIS
testMEMORY
testMAXDIM
testNROOTS
testIROOT
