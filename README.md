Virtual Molecule Screener

This is a set of wrappers around the ORCA quantum chemistry software.
It parses a user's request (such as: light absorption) and submits
the appropriate instructions to ORCA, along with a database of molecules,
and interprets the results.
