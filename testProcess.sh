#!/bin/sh

NAME="excited.dft.N"
SUFFIX="log"
MAX=10240

OUTFILE="$NAME.dat"
rm -f $OUTFILE
i=1
while test $i -le $MAX
do
	FILE="$NAME.$i.$SUFFIX"
	if test -e $FILE; then
		OREAL=$(tail -n 7 $FILE | head -n 1 | cut -d " " -f 2)
		OUSER=$(tail -n 6 $FILE | head -n 1 | cut -d " " -f 2)
		OSYS=$(tail -n 5 $FILE | head -n 1 | cut -d " " -f 2)
		REAL=$(tail -n 3 $FILE | head -n 1 | cut -d " " -f 2)
		USER=$(tail -n 2 $FILE | head -n 1 | cut -d " " -f 2)
		SYS=$(tail -n 1 $FILE | cut -d " " -f 2)
		echo "$i $REAL $USER $SYS $OREAL $OUSER $OSYS" >> $OUTFILE
	fi
	i=$((i+1))
done
